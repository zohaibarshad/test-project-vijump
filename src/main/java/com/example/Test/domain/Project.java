package com.example.Test.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "PROJECT")
public class Project {
	
	 
	    public Project() {
		super();
		// TODO Auto-generated constructor stub
	}

		@Id
	    @GeneratedValue
	    @Column(name = "Id", nullable = false)
	    private Long id;
	    
		@Column(name = "Name", length = 64, nullable = false)
	    private String name;
	    
	    @Column(name = "Description", length = 64, nullable = false)
	    private String description;
	 
	    
		public Project(String name, String description) {
			super();
			this.name = name;
			this.description = description;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

}
