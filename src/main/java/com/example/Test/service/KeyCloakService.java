package com.example.Test.service;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.representations.idm.RoleRepresentation;
import org.springframework.stereotype.Service;

import com.example.Test.YAMLConfig;

@Service
public class KeyCloakService {

	YAMLConfig myConfig;

	public void saveRole(String name, String description) {
		// The URL to access KeyCloak
		String serverUrl = myConfig.getKeycloakServerUrl();
		// Created Real in KeyCloak
		String realm = myConfig.getKeycloakRealm();
		// Created Client Id in KeyCloak
		String clientId = myConfig.getKeycloakClient();
		// getting KeyCloak
		Keycloak keycloak = KeycloakBuilder.builder().serverUrl(serverUrl).realm(realm)
				.grantType(OAuth2Constants.PASSWORD).clientId(clientId).username("user1").password("password").build();

		// Making a new Role
		RoleRepresentation role = new RoleRepresentation();
		role.setName(name);
		role.setDescription(description);
		// Adding new Role to KeyCloak
		keycloak.realm(realm).roles().create(role);

	}

}
