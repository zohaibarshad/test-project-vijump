package com.example.Test.service;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Test.controller.projectController;
import com.example.Test.domain.Project;
import com.example.Test.repository.ProjectRepository;

import lombok.extern.slf4j.Slf4j;

import org.keycloak.OAuth2Constants;
//import org.keycloak.adapters.spi.HttpFacade.Response;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.admin.client.resource.RolesResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.RolesRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

@Slf4j
@Service
public class ProjectService {
	@Autowired
	ProjectRepository projectRepo;

	@Autowired
	KeyCloakService keyCloakService;

	public void Init() {
		// Add some dummy projects in db (H2 DB) at start
		if (projectRepo.count() == 0) {
			log.info("Innitializing DB with Dummy data");
			
			Project project = new Project("Project1", " Prodject 1 Description");
			projectRepo.save(project);

			project = new Project("Project2", " Prodject 2 Description");
			projectRepo.save(project);
		}

	}

	public List<Project> getProjects() {
		log.info("Fetching All Projects");
		return (List<Project>) projectRepo.findAll();
	}

	public String saveProject(String name, String description) {
		log.info("Saving a Project");
		if (name != "" && description != "") {
			if (projectRepo.save(new Project(name, description)) != null) {
				// Saving New Created Project AS A New Role In KeyCloak
				keyCloakService.saveRole(name, description);
				log.info("Project Saved Successfully");
				return "Added Successfully";
			} else {
				log.info("Error occured while saving Project ");
				return "Encountered Some Problem While Saving Project";

			}
		} else {
			log.info("Error occured while saving Project ");
			return "Encountered Some Problem While Saving Project";
		}
	}

}
