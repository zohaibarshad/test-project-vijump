package com.example.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.example.Test.service.ProjectService;

@Component
public class Init implements ApplicationRunner {

	@Autowired
	ProjectService projectService;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		projectService.Init();
	}

}
