package com.example.Test.controller;

import java.security.Principal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.access.prepost.PreAuthorize;
//import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.Test.domain.Project;
import com.example.Test.service.ProjectService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class projectController {

	@Autowired
	ProjectService projectService;

	@GetMapping("/get-unsecured-projects")
	public List<Project> getSecuredProjects() {
		return projectService.getProjects();
	}

	@GetMapping("/get-projects")
	@PreAuthorize("hasRole('user')")
	public List<Project> getProjects(Principal principal, Model model) {
		// log.info(principal.getName());
		return projectService.getProjects();
	}

	@RequestMapping(value = "/add-project", method = RequestMethod.POST)
	public String requestOTP(@RequestBody Map<String, String> jsonBody) {
		return projectService.saveProject(jsonBody.get("Name"), jsonBody.get("Description"));
	}

}
