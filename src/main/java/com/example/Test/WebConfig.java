package com.example.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

	@Autowired
	private YAMLConfig myConfig;

	@Override
	public void addCorsMappings(CorsRegistry registry) {

		YAMLConfig YML = new YAMLConfig();

		// this lines allow the local host angular server to use 
		registry.addMapping("/**").allowedOrigins(myConfig.getServers().get(0));
	}
}