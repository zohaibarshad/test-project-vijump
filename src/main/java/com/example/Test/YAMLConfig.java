package com.example.Test;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Configuration
@ConfigurationProperties
@EnableConfigurationProperties

@Data
public class YAMLConfig {

	private String name;
	private String environment;
	private String keycloakServerUrl;
	private String keycloakRealm;
	private String keycloakClient;
	private List<String> servers = new ArrayList<>();

}