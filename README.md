Task 1

Download and install docker.
Run command ‘ docker run jboss/keycloak’ on cmd
Open Kitematic.exe
Select running Keycloak container
Go to settings and click General Tab
Add a user name for KEY_CLOAK user
Add a password for KEY_CLOAK user
Now click Hostname/Ports tab
Add 8080 as a port ahead of ip ‘192.168.99.100’
Access KeyCloak by URL ‘192.168.99.100 :8080’ by using the username and password set at steps 6 and 7.
Create a realm using Name as ‘TestProjectKeyCloak’
Create a Client with id ‘test-app’ and set Valid redirect URI ‘http://localhost :8080’
Create a role with name ‘user’
Create a user with name ‘user1’. Set credentials of user to ‘password’. Go to role mapping TAB and map ‘user’ role to the user
Run spring application and access a url ‘http://localhost :8080/getSecuredProjects’
You will be prompted to give username and password. Add the credentials username = user1, password = password (the user created on keycloak)
You will see the predefined Projects in db.
TASK 2

A page is also created on Angular 6 which shows lists of project by calling api from spring boot. (So you need the spring project running on localhost :8080 and also peroform the keycloak setup explained in Task1)
After running angular app (use npm install and the ng serve to run the app )you will see list of projects and above it . Asumming that you do have angular setup with you. if not then take this as refference https://angular.io/guide/quickstart